const { series, parallel, watch, src, dest } = require("gulp");
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass")(require("sass"));

const serv = () => {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
};

const bsReload = (cb) => {
  browserSync.reload();
  cb();
};

const watcher = (cb) => {
  watch("./index.html", bsReload);
  watch("./src/scss/*.scss", styles);
  watch("./src/js/*.js", js);
  cb();
};

const stylesDev = () => {
  return (
    src("./src/scss/style.scss")
      .pipe(sass().on("error", sass.logError))
      .pipe(cleanCss())
      .pipe(
        rename({
          extname: ".min.css",
        })
      )
      .pipe(dest("./dist/styles/"))
      .pipe(browserSync.stream())
  );
};

const jsDev = () => {
  return src("./src/js/*.js")
    .pipe(dest("./dist/js/"))
    .pipe(browserSync.stream());
};

exports.dev = parallel (watcher, serv, series (stylesDev, jsDev))




// BUILD

const del = require("del");
const autoprefixer = require("gulp-autoprefixer");
const rename = require("gulp-rename");
const imgMin = require("gulp-imagemin");
const cleanCss = require("gulp-clean-css");

const reset = () => {
  return del("./dist/**/*.*");
};

const images = () => {
  return src("./src/img/**/*.*").pipe(imgMin()).pipe(dest("./dist/img/"));
};

const styles = () => {
  return (
    src("./src/scss/style.scss")
      .pipe(sass().on("error", sass.logError))
      .pipe(
        autoprefixer({
          grid: true,
          overrideBrowserslist: ["last 3 versions"],
          cascade: true,
        })
      )
      .pipe(cleanCss())
      .pipe(
        rename({
          extname: ".min.css",
        })
      )
      .pipe(dest("./dist/styles/"))
      .pipe(browserSync.stream())
  );
};


const js = () => {
  return src("./src/js/*.js")
    .pipe(dest("./dist/js/"))
    .pipe(browserSync.stream());
};



exports.build = series(reset, styles, js, images);
