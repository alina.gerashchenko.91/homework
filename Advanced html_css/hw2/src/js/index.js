const show = document.querySelector(".show");
const close = document.querySelector(".close");
const menuBtn = document.querySelector(".nav");
const menu = document.querySelector(".nav__list");

show.addEventListener("click", function () {
  menu.style.display = "block";
  show.style.display = "none";
  close.style.display = "block";
  console.log("первый");
});

close.addEventListener("click", function () {
  menu.style.display = "none";
  close.style.display = "none";
  show.style.display = "block";
  console.log("второй");
});
