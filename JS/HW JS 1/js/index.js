let userName = null;
let userAge = null;

do {
  userName = prompt("What is your name?");
} while (!userName || !userName.trim() || !Number.isNaN(+userName));
do {
  userAge = +prompt("How old are you?");
} while (!userAge || Number.isNaN(userAge));

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
  if (confirm("Are you sure you want to continue?")) {
    alert(`Welcome, ${userName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert(`Welcome, ${userName}`);
}
