const input = document.querySelector("input");
const priceArea = document.querySelector("div");
const allertArea = document.createElement("p");
input.after(allertArea);


input.addEventListener("focus", () => input.classList.add("focusInput"), true);

input.addEventListener("blur", () => {
  input.classList.remove("focusInput"), true;
  input.classList.remove("error"), true;

  if (input.value > 0) {
    const userInputs = document.createElement("span");
    priceArea.append(userInputs);
    userInputs.innerText = `Текущая цена: ${input.value}`;
    input.classList.add("right"), true;
    allertArea.innerText = '';

    const btn = document.createElement("button");
    btn.innerText = "x";
    userInputs.append(btn);
    btn.addEventListener ('click', () => {
      btn.parentNode.parentNode.removeChild(btn.parentNode);
      input.classList.remove("right"), true;
      input.value = "";
    });
  } else if (input.value <= 0) {
    input.classList.add("error"), true;
    input.classList.remove("right"), true;

    allertArea.innerText = "Please enter correct price";
  }
});
console.log(input.getCompudetStyle);
