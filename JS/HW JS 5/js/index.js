function createNewUser() {
  const userName = prompt("What is your name?");
  const userLastName = prompt("What is your last name?");
  const userbirthday = prompt("Enter your birthday in DD.MM.YYYY format pls");

  const newUser = {
    name: userName,
    lastName: userLastName,
    birthday: new Date(`${userbirthday}`),
    getLogin: function () {
      return this.name.substr(0, 1).toLowerCase() + this.lastName.toLowerCase();
    },
    getAge: function () {
      const currentDate = new Date();
      return currentDate.getFullYear() - this.birthday.getFullYear();
    },
    getPassword: function () {
      return (
        this.name.substr(0, 1).toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.getFullYear()
      );
    },
  };
  return newUser;
}

let myUser = createNewUser();
console.log(myUser.getLogin());
console.log(myUser.getAge());
console.log(myUser.getPassword());
