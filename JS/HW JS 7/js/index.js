const arr = ["hello", "sfgsdg", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function addList(arr, place) {
  const list = document.createElement("ul");
  place.append(list);

  const newArr = arr.map((i) => {
    const listItem = `<li>${i}</li>`;
    return listItem;
  });

  list.innerHTML = newArr.join(" ");
  return list;
}

addList(arr, document.body);
