const btnTheme = document.querySelector(".btn-theme");
const link = document.querySelector("#theme");

btnTheme.addEventListener("click", switchTheme);

function switchTheme() {
  if (link.getAttribute("href") == "./css/style-dark.css") {
    link.setAttribute("href", "./css/styles.css");
    localStorage.setItem("theme", "light");
  } else {
    link.setAttribute("href", "./css/style-dark.css");
    localStorage.setItem("theme", "dark");
  }
}

window.addEventListener("load", function () {
  if (localStorage.getItem("theme") === "dark") {
    link.setAttribute("href", "./css/style-dark.css");
  } else if (localStorage.getItem("theme") === "light") {
    link.setAttribute("href", "./css/styles.css");
  }
});
