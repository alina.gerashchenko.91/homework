let userNum;

do {
  userNum = +prompt("Enter the number");
} while (!userNum || Number.isNaN(userNum) || !Number.isInteger(userNum));


if (userNum < 5) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 0; i <= userNum; i++) {
    if (i % 5 === 0) {
      {
        console.log(i);
      }
    }
  }
}
