const startBtn = document.querySelector("#start");
const stopBtn = document.querySelector("#stop");
let currentSlide = 0;
let showing = true;
const slides = document.querySelectorAll("#list .slide");

startBtn.addEventListener("click", start);

stopBtn.addEventListener("click", pause);

let go = setInterval(carusel, 3000);

function carusel() {
  slides[currentSlide].className = "slide";

  currentSlide = (currentSlide + 1) % slides.length;
  slides[currentSlide].className = "slide show";
}

function pause() {
  clearInterval(go);
  showing = false;
}

function start() {
  if (!showing) {
    go = setInterval(carusel, 3000);
    showing = true;
  }
}
