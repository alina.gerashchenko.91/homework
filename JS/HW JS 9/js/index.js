const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsItem = document.querySelectorAll(".tabs-item");

tabsTitle.forEach(function (e) {
  e.addEventListener("click", () => {
    const tabsId = e.getAttribute("data-tab");

    const activeItem = document.querySelector(tabsId);

    tabsTitle.forEach(function (e) {
      e.classList.remove("active");
    });

    tabsItem.forEach(function (e) {
      e.classList.add("hide");
    });

    e.classList.add("active");
    activeItem.classList.remove("hide");
  });
});

window.addEventListener("load", () => {
  const firstTitle = document.querySelector(".tabs-title");
  const firstItem = document.querySelector(".tabs-item");

  firstTitle.classList.add("active");
  firstItem.classList.remove("hide");
});
