import React, {Component} from "react";
import './Button.scss'

class Button extends Component {

    render () {
const {action, btnText, btnColor} = this.props; 

        return (
            <button className="btn" onClick={action} style={{backgroundColor: btnColor}}>{btnText}</button>
        )
    }
}

export default Button;