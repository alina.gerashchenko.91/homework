import React, { Component } from "react";

import './Modal.scss';

class Modal extends Component {

  render() {
    const {title, text, actions, background, click} = this.props;
    return (
      <div className="modal" onClick={click}>
        <div className="modal-item" 
        style={{backgroundColor: background}}
        onClick={(e) => e.stopPropagation()}>
          <h1 className="modal-item_title">{title}</h1>
          <p className="modal-item_text">{text}</p>
          {actions}
        </div>
      </div>
    );
  }
}

export default Modal;
