import React, { Component } from "react";
import Modal from "./components/Modal";
import Button from "./components/Button";
import "./App.css";

class App extends Component {
  state = {
    first: {
      title: "First Modal is open",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
      background: "#d39ae6",
      
    },

    second: {
      title: "Second Modal is open",
      text: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
      background: "#bf94e4",
    },
    firstBtn: "Open First Modal",
    secondBtn: "Open Second Modal",
    isFirstModal: false,
    isSecondModal: false,

      };


  openModalFirst = () => {

    this.setState ((prevState) => {
      return {
        ...prevState,
        isFirstModal: !prevState.isFirstModal
      }
    })
  };

  openModalSecond = () => {
        this.setState ((prevState) => {
          return {
            ...prevState,
            isSecondModal: !prevState.isSecondModal
          }
        })
      };

closeModal = (e) => {
  
  this.setState ({
    isFirstModal: false
  })
  this.setState ({
    isSecondModal: false
  })
  
}

  render() {
    const { first, second, firstBtn, secondBtn, isFirstModal, isSecondModal } = this.state;
    return (
      <>
      <Button 
        btnText={firstBtn} 
        btnColor="#c04ee5"
        action={() => this.openModalFirst()}
        />
      <Button btnText={secondBtn}
      btnColor="#bf94e4"
      action={() => this.openModalSecond()}
      />
      {isFirstModal && <Modal 
      click={() => this.closeModal()}
          title={first.title} 
          text={first.text}
          background={first.background}
          actions={
           <>
           <div className="btn-wrap">
            <Button background={first.btnClose} btnText="CLOSE"  btnColor="#bf94e4" action={() => this.closeModal()}/>
            <Button background={first.btnOk} btnText="CANCEL" btnColor="#ee82ee" action={() => alert("Press CLOSE button to close Modal Window")}/>
            </div>
            <svg
                  height="20px"
                  width="20px"
                  viewBox="0 0 500 500"
                  onClick={() => this.closeModal()}
                >
                  <path d="M505.943,6.058c-8.077-8.077-21.172-8.077-29.249,0L6.058,476.693c-8.077,8.077-8.077,21.172,0,29.249    C10.096,509.982,15.39,512,20.683,512c5.293,0,10.586-2.019,14.625-6.059L505.943,35.306    C514.019,27.23,514.019,14.135,505.943,6.058z" />

                  <path d="M505.942,476.694L35.306,6.059c-8.076-8.077-21.172-8.077-29.248,0c-8.077,8.076-8.077,21.171,0,29.248l470.636,470.636    c4.038,4.039,9.332,6.058,14.625,6.058c5.293,0,10.587-2.019,14.624-6.057C514.018,497.866,514.018,484.771,505.942,476.694z" />
                </svg>

          </>
          }
            />}
      {isSecondModal && <Modal 
      click={() => this.closeModal()}
          title={second.title} 
          text={second.text}
          background={second.background}
          actions={
            <>
            <div className="btn-wrap">
            <Button background={first.btnClose} btnText="CLOSE"  btnColor="#f984ef" action={() => this.closeModal()}/>
            <Button background={first.btnOk} btnText="CANCEL" btnColor="#d19fe8" action={() => alert("Press CLOSE button to close Modal Window")}/>
            </div>
             <svg
                  height="20px"
                  width="20px"
                  viewBox="0 0 500 500"
                  onClick={() => this.closeModal()}
                >
                  <path d="M505.943,6.058c-8.077-8.077-21.172-8.077-29.249,0L6.058,476.693c-8.077,8.077-8.077,21.172,0,29.249    C10.096,509.982,15.39,512,20.683,512c5.293,0,10.586-2.019,14.625-6.059L505.943,35.306    C514.019,27.23,514.019,14.135,505.943,6.058z" />

                  <path d="M505.942,476.694L35.306,6.059c-8.076-8.077-21.172-8.077-29.248,0c-8.077,8.076-8.077,21.171,0,29.248l470.636,470.636    c4.038,4.039,9.332,6.058,14.625,6.058c5.293,0,10.587-2.019,14.624-6.057C514.018,497.866,514.018,484.771,505.942,476.694z" />
                </svg>
          </>
          }
            />}
      </>
      
    // <Modal 
    // title={first.title} 
    // text={first.text}
    // background={second.background}
    // />
    )
  }
}

export default App;
