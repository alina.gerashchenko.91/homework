import { useState, useEffect } from "react";
import { Route, Routes } from "react-router-dom";

import Button from "./components/Button";
import Modal from "./components/Modal";
import Header from "./components/Header";
import Cards from "./pages/CardsPage";

import FavoritesPage from "./pages/FavoritesPage";
import BasketPage from "./pages/BasketPage";

import { ReactComponent as Close } from "./components/icons/close.svg";


import "./index.scss";

function App() {
  const [basket, setBasket] = useState(JSON.parse(localStorage.getItem("basket")) || []);
  const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem("favorite")) || []);
  const [isModal, setIsModal] = useState(false);
  const [isFav, setIsFav] = useState(false);
  const [card, setCard] = useState(JSON.parse(localStorage.getItem("card")) || {});
  const [countBasket, setCountBasket] = useState(0);
  const [countFavorite, setCountFavorite] = useState(0);
  const [isModalSecond, setIsModalSecond] = useState(false);
  

  useEffect(() => {
 
    localStorage.setItem("favorite",JSON.stringify(favorite))
    localStorage.setItem("basket",JSON.stringify(basket))
    localStorage.setItem("card",JSON.stringify(card))

    if (JSON.parse(localStorage.getItem("favorite"))) {
        setCountFavorite(JSON.parse(localStorage.getItem("favorite")).length)
      }

    if (JSON.parse(localStorage.getItem("basket"))) {
      setCountBasket(JSON.parse(localStorage.getItem("basket")).length)
    }
    
   }, [favorite, basket, card]);

  const modalOpen = () => {
    setIsModal(true);
  };

  const secondModalOpen = () => {
    setIsModalSecond(true)
  }

  const closeModal = () => {
    setIsModal(false);
    setIsModalSecond(false);
  };

  const handlerCard = (card) => {
    setCard({...card});
  };

  const addToBasket = (card) => {
    if (!basket.find((item) => item.id === card.id)) {
      setBasket([...basket, card]);
     
      closeModal();
    } else {
      alert("Товар вже додано до кошика. Ви можете змінити кількість товарів у відповідному розділі")
      closeModal();
    }
  };

  const removeFromBasket = (card) => {
    if (basket.find((item) => item.id === card.id))  {
      setBasket(basket.filter(item => item.id !== card.id));
      closeModal();
      console.log("Card was deleted");
    }
  }


  const addToFavorites = (card) => {
    if (!favorite.find((item) => item.id === card.id)) {
      setFavorite([... favorite, card])
      setIsFav(true)
    } else {
      deleteFavorite(card)
    }
   }

   const deleteFavorite = (card) => {
    setFavorite(favorite.filter(item => item.id !== card.id))
    setIsFav(false)
   }

  

  return (
    <>
    <Header countBasket={countBasket} countHeart={countFavorite} />
      <Routes>
        <Route path="/" element={ <Cards
      addToFavorites={addToFavorites}
      modalOpen={modalOpen}
      isFav={isFav}
      handlerCard={handlerCard}
    />} />
        <Route path="/basket" element={<BasketPage basket={basket} 
        handlerCard={handlerCard}
        removeFromBasket={removeFromBasket}
        isModalSecond={isModalSecond}
        closeModal={closeModal}
        secondModalOpen={secondModalOpen}
        />}/>
        <Route path="/favorites" element={<FavoritesPage favorite={favorite} removeFromFavorites={deleteFavorite}
        />} />
      </Routes>

      {isModal && (
        <Modal
          backgroundColor="#D5D8E3"
          text="Якщо ви хочете додати товар до кошика, натисніть ОК. Якщо ні - можете закрити вікно."
          header="Додати товар до кошика"
          onClick={() => closeModal()}
          actions={
            <div className="buttons-wrap">
              <Button
                text="OK"
                backgroundColor="#95979E"
                onClick={() => addToBasket(card)}
              />
              <Button
                text="Cancel"
                backgroundColor="#95979E"
                onClick={() => closeModal()}
              />
              <Close onClick={() => closeModal()} />
            </div>
          }
        />
      )}
    </>
  );
}

export default App;