import React, { useEffect, useState } from "react";
import sendRequest from "../../helpers";
import Card from "../../components/Card/Card";
import PropTypes from "prop-types";

function Cards(props) {
  const [cards, setCards] = useState([])

        const {handlerCard, addToFavorites, modalOpen } = props;

        const showCards = () => {
          sendRequest(`${process.env.PUBLIC_URL}/productsArray.json`).then((data) => {
            setCards(data);
          });
        };

        useEffect(() => {
          showCards()
        }, [])

        const cardItem = cards.map((card) => (
          <Card
            key={card.id}
            currentCard={card}
            handlerCard={handlerCard}
            addToFavorites={() => addToFavorites(card)}
            modalOpen={modalOpen}
          />
        ));
    
        return <div className="products">{cardItem}</div>;
      
}

Cards.propTypes = {
  handlerBasket: PropTypes.func,
  addToFavorites: PropTypes.func,
  modalOpen: PropTypes.func,
  handlerCard: PropTypes.func
};

export default Cards;
