import { useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  actionModal,
  actionIsFav,
  actionBasket,
  actionCard,
  actionFavorite,
  actionDeleteFavorite,
} from "./actions";

import {
  selectorBasket,
  selectorFavorite,
  selectorCard,
  selectorIsModal,
  selectorIsFav,
} from "./selectors";

import Button from "./components/Button";
import Modal from "./components/Modal";
import Header from "./components/Header";
import Cards from "./pages/CardsPage";

import FavoritesPage from "./pages/FavoritesPage";
import BasketPage from "./pages/BasketPage";

import ToggleProvider from "./context";

import { ReactComponent as Close } from "./components/icons/close.svg";

import "./index.scss";

const App = () => {

  const dispatch = useDispatch();
  const card = useSelector(selectorCard); // to CardPage
  const basket = useSelector(selectorBasket);
  const favorite = useSelector(selectorFavorite);
  const isModal = useSelector(selectorIsModal);
  const isFav = useSelector(selectorIsFav);

useEffect(() => {
  localStorage.setItem("favorite", JSON.stringify(favorite));
    localStorage.setItem("basket", JSON.stringify(basket));
    localStorage.setItem("card", JSON.stringify(card));

},  [favorite, basket, card])

  const modalToggle = () => {
    dispatch(actionModal());
  };

  const handlerCard = (card) => {
    dispatch(actionCard(card));
  };

  const addToBasket = (card) => {
    if (!basket.find((item) => item.id === card.id)) {
      dispatch(actionBasket(card));
      modalToggle();
    } else {
      alert(
        "Товар вже додано до кошика. Ви можете змінити кількість товарів у відповідному розділі"
      );
      modalToggle();
    }
  };

  const addToFavorites = (card) => {
    if (!favorite.find((item) => item.id === card.id)) {
      dispatch(actionFavorite(card));
   //   dispatch(actionIsFav());
    } else {
      deleteFavorite(card);
      
    }
  };

  const deleteFavorite = (card) => {
    dispatch(actionDeleteFavorite(card));
 //   dispatch(actionIsFav());
  };

  return (
    <ToggleProvider>
        <Header/>
      <Routes>
        <Route
          path="/"
          element={
            <Cards
              addToFavorites={addToFavorites}
              modalOpen={modalToggle}
              isFav={isFav}
              handlerCard={handlerCard}
            />
          }
        />
        <Route
          path="/basket"
          element={<BasketPage/>}/>
        <Route
          path="/favorites"
          element={<FavoritesPage/>}/>
      </Routes>
      
      {isModal && (
        <Modal
          backgroundColor="#D5D8E3"
          text="Якщо ви хочете додати товар до кошика, натисніть ОК. Якщо ні - можете закрити вікно."
          header="Додати товар до кошика"
          onClick={() => modalToggle()}
          actions={
            <div className="buttons-wrap">
              <Button
                text="OK"
                backgroundColor="#95979E"
                onClick={() => addToBasket(card)}
              />
              <Button
                text="Cancel"
                backgroundColor="#95979E"
                onClick={() => modalToggle()}
              />
              <Close onClick={() => modalToggle()} />
            </div>
          }
        />
      )}
    </ToggleProvider>
  );
}

export default App;
