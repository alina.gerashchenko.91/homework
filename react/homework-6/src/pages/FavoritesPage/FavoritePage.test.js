import { render, screen, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import "@testing-library/jest-dom";
import store from "../../store";
import FavoritesPage from "./FavoritesPage";

describe("FavoritesPage", () => {
  test("Snapshot FavoritesPage", () => {
    const favoritesPage = render(
      <Provider store={store}>
        <FavoritesPage currentCard={{}} />
      </Provider>
    );

    expect(favoritesPage).toMatchSnapshot();
  });

  test("Checked beInDoc", () => {
    const { container } = render(
      <Provider store={store}>
        <FavoritesPage currentCard={{}} />
      </Provider>
    );
    expect(container).toBeInTheDocument();
  });

  test("Checked class", () => {
    const { container } = render(
      <Provider store={store}>
        <FavoritesPage currentCard={{}} />
      </Provider>
    );
    expect(container.firstChild).toHaveClass("container");
  });

  test("Checked Fav Button", () => {
    const { container } = render(
      <Provider store={store}>
        <FavoritesPage currentCard={{}} />
      </Provider>
    );

    const favBtn = container.querySelector("svg");
    waitFor(() => expect(favBtn).toBeInTheDocument());
  });

  
});
