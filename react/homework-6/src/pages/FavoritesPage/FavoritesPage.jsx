import { useSelector } from "react-redux/";
import { selectorFavorite } from "../../selectors";


import Heart from "../../components/icons/Heart";
import PropTypes from "prop-types";

import { useDispatch } from "react-redux";
import {actionDeleteFavorite, actionIsFav} from "../../actions"

function FavoritesPage() {
const favorite = useSelector(selectorFavorite);
const dispatch = useDispatch()
const deleteFavorite = (card) => {
  dispatch(actionDeleteFavorite(card));
  dispatch(actionIsFav());
};

  const favItem = favorite?.map((currentCard) => (
    <div className="products_card" id={currentCard.id} key={currentCard.id}>
      <div className="card_icon">
        <img
          className="card_icon_img"
          src={currentCard.url}
          alt={currentCard.title}
        />
      </div>
      <div className="card_description">
        <p className="title">
          <b>Назва:</b> {currentCard.title}
        </p>
        <p className="price">
          <b>Ціна:</b> {currentCard.price}
        </p>
        <p>
          <b>Колір:</b>
          <span
            className="color"
            style={{ backgroundColor: currentCard.color }}
          ></span>
        </p>
        <p>
          <b>Артикль:</b> {currentCard.article}
        </p>
      </div>
      <div className="buttons-wrap">
        <Heart
          data-testid="fav-btn"
          fill="pink"
          onClick={() => {
            deleteFavorite(currentCard);
          }}
        />
      </div>
    </div>
  ));

  return (
    <div className="container">
   <div className="products">{favItem}</div>
    </div>
  );
}

FavoritesPage.propTypes = {
  removeFromFavorites: PropTypes.func,
  favorite: PropTypes.array,
};

export default FavoritesPage;
