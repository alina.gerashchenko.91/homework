import Button from "../../components/Button";
import Modal from "../../components/Modal";
import FormOrder from "../../components/Form/FormOrder";

import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";

import { selectorBasket, selectorIsModalForm, selectorIsModalSecond, selectorCard } from "../../selectors";
import { actionModalForm, actionDeleteBasket, actionModalSecondOpen, actionModalSecondClose, actionCard} from "../../actions";
import { ReactComponent as Close } from "../../components/icons/close.svg";

import "./BasketPage.scss";

function BasketPage() {
  const basket = useSelector(selectorBasket);
  const card = useSelector(selectorCard);
  const isModalSecond = useSelector(selectorIsModalSecond);
  const isModalForm = useSelector(selectorIsModalForm);
  const dispatch = useDispatch();

  const handlerCard = (card) => {
    dispatch(actionCard(card));
  };

  const removeFromBasket = (card) => {
      dispatch(actionDeleteBasket(card));
      dispatch(actionModalSecondClose())
    
  };

  const basketItem = basket?.map((card) => (
    <div
      className="products_card basket"
      id={card.id}
      key={card.id}
    >
      <div className="card_icon">
        <img
          className="card_icon_img"
          src={card.url}
          alt={card.title}
        />
      </div>
      <div className="card_description">
        <p className="title">
          <b>Назва:</b> {card.title}
        </p>
        <p className="price">
          <b>Ціна:</b> {card.price}
        </p>
        <p>
          <b>Колір:</b>
          <span
            className="color"
            style={{ backgroundColor: card.color }}
          ></span>
        </p>
        <p>
          <b>Артикль:</b> {card.article}
        </p>
      </div>
      <div className="buttons-wrap">
        <Close
        className="delete"
          onClick={() => {
            //setCurrentCard(currentCard);
            //console.log(currentCard);
            handlerCard(card);
            dispatch(actionModalSecondOpen());
          }}
        />
      </div>
    </div>
  ));

  return (
    <div className="container">
       <div className="products">{basketItem}</div>
      <div className="open_form">
        <Button
        type="button"
        onClick={() => dispatch(actionModalForm())}
        text={!isModalForm ? "Оформити замовлення" : "Закрити вікно замовлення"}
        
        />
      </div>
{isModalForm && <FormOrder/>}
      
      {isModalSecond && (
        <Modal
          backgroundColor="#D5D8E3"
          text="Товар додано до кошика. Якщо хочете видалити його, натисніть ОК"
          header="Видалення товару"
          //onClick={modalSecondToggle()}
          actions={
            <div className="buttons-wrap">
              <Button
                text="OK"
                backgroundColor="#95979E"
                onClick={() => {
                  removeFromBasket(card);
                }}
              />
              <Button
                text="Cancel"
                backgroundColor="#95979E"
                onClick={() => dispatch(actionModalSecondClose())}
              />
              <Close onClick={() => dispatch(actionModalSecondClose())} />
            </div>
          }
        />
      )}
      
    </div>
  );
}

BasketPage.propTypes = {
  removeFromFavorites: PropTypes.func,
  favorite: PropTypes.array,
  basket: PropTypes.array,
  secondModalOpen: PropTypes.func,
  removeFromBasket: PropTypes.func,
  handlerCard: PropTypes.func,
  closeModal: PropTypes.func,
  modalSecondToggle: PropTypes.func,
};

export default BasketPage;
