import { render, screen, waitFor } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import { Provider } from "react-redux";
import store from "../../store";
import BasketPage from "./BasketPage";

describe("BacketPage", () => {
  test("Snapshot BacketPage", () => {
    const basketPage = render(
      <Provider store={store}>
        <BasketPage currentCard={{}} />
      </Provider>
    );

    expect(basketPage).toMatchSnapshot();
  });

  test("Checked Class", () => {
    const {container} = render(
      <Provider store={store}>
        <BasketPage currentCard={{}} />
      </Provider>
    );
    expect(container.firstChild).toHaveClass("container");
  });

  test("Checked delete btn", () => {
    const {container} = render(
      <Provider store={store}>
        <BasketPage currentCard={{}} />
      </Provider>
    );
    const deleteBnt = container.querySelector('button');
waitFor (() => expect(deleteBnt).toBeInTheDocument())
  })

});