import {render, waitFor, fireEvent} from "@testing-library/react";
import { Provider } from "react-redux";
import "@testing-library/jest-dom";
import store from "../../store";
import CardsPage from ".";

describe("CardPage", () => {
    test("CardPage render", () => {
        const cardPage = render(<Provider store={store}><CardsPage/></Provider>);
        expect(cardPage).toMatchSnapshot() 

    })
    
    test('Checked toBeInDoc', () => {
        const {container} = render(<Provider store={store}><CardsPage/></Provider>);
        expect(container).toBeInTheDocument()
    })


    test('Checked btn list', () => {
        const {getByText} = render(<Provider store={store}><CardsPage/></Provider>);
        
        const btnBasket = getByText(/Показати у списку/i)
        expect(btnBasket).toBeInTheDocument()
        
        //expect(container.firstChild).toHaveClass("container");
    })
//     test("Checked page toggler", () => {
//         const {container, getByTestId} = render(<Provider store={store}><CardsPage/></Provider>)
    
//     const togglerBtn = getByTestId("page-toggler");
//     const div = container.querySelector('div');
//     // expect(div).toHaveCass("products")
//     expect(div).toBeInTheDocument()
//    })
})
