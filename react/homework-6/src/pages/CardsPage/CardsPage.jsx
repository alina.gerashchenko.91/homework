import { useSelector, useDispatch } from "react-redux";
import { useEffect, useContext} from "react";
import PropTypes from "prop-types";

import Card from "../../components/Card/Card";
import CardList from "../../components/Card/CardList";
import { actionFetchCards } from "../../actions";
import { selectorCards } from "../../selectors";
import Button from "../../components/Button";


import {ToggleContext} from "../../context"


import "./CardsPage.scss"

function Cards({handlerCard, addToFavorites, modalOpen}) {
  
  const cards = useSelector(selectorCards);
  const dispatch = useDispatch();
  const {isList, toggleCards} = useContext(ToggleContext);

  useEffect(() => {
    dispatch(actionFetchCards());
  }, [])

  const cardItem = cards?.map((card) => (
    <Card
      key={card.id}
      currentCard={card}
      handlerCard={handlerCard}
      addToFavorites={() => addToFavorites(card)}
      modalOpen={modalOpen}
    />
  ));

  const cardList = cards.map((card) => (
    <CardList
      key={card.id}
      currentCard={card}
      handlerCard={handlerCard}
      addToFavorites={() => addToFavorites(card)}
      modalOpen={modalOpen}
    />
  ));

  return (
  <div className="container">
  <div className="button-wrp">
    <Button 
      data-testid="page-toggler"
      text={isList ? "Показати плитку" : "Показати у списку"}
      onClick={toggleCards}
  />
  </div>
  {isList && <div className="products_list">{cardList}</div>}
  {!isList && <div className="products">{cardItem}</div>}
  </div>
  )
}

Cards.propTypes = {
  handlerBasket: PropTypes.func,
  addToFavorites: PropTypes.func,
  modalOpen: PropTypes.func,
  handlerCard: PropTypes.func,
};

export default Cards;
