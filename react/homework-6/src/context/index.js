import { useState, createContext } from "react";
export const ToggleContext = createContext({});

 const ToggleProvider = ({ children }) => {

const [isList, setIsList] = useState(false);

const toggleCards = () => {
    setIsList(!isList);
}

  return (
    <ToggleContext.Provider value={{isList, toggleCards}}>
        {children}
    </ToggleContext.Provider>
    )
};

export default ToggleProvider;