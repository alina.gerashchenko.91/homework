import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import App from "./App";

describe("App", () => {
  test("Snapshot App", () => {
    const app = render(
      <Provider store={store}>
        <BrowserRouter>
        <App />
        </BrowserRouter>
      </Provider>
    );
    expect(app).toMatchSnapshot();
  });

});
