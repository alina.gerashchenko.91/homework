import rootReduser from "./index";
import * as actions from "../actions";
import {initialState} from './index';
import {appReducer} from 'redux'

describe("Redusers functions", () => {
  it("actionCvData", () => {
    const mockData = {
      name: "Alina",
      lastName: "Holovko",
      age: "31",
    };
    const mockExpected = {
      cv: {
        name: "Alina",
        lastName: "Holovko",
        age: "31",
      },
    };

    expect(rootReduser({}, actions.actionCvData(mockData))).toStrictEqual(mockExpected); // строге порівняння
  });

  it("actionCleanBasket", () => {
    const mockData = []; 
    const mockExpected = {
        basket: []
    };
    expect(rootReduser({}, actions.actionCleanBasket(mockData))).toStrictEqual(mockExpected)
  });

});

