import Input from "../components/Input";
import TextArea from "../components/TextArea";
import { Formik, Form, useFormikContext } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { selectorBasket, selectorCvData, selectorIsModalForm } from "../../../selectors";
import { actionCvData, actionModalForm, actionCleanBasket } from "../../../actions";

import {validationSchema} from "./validation"

import {ReactComponent as Close} from "../../icons/close.svg"

import Button from "../../Button";

import "./FormOrder.scss";

const FormOrder = () => {
  const dispatch = useDispatch();
  const cvData = useSelector(selectorCvData);
  const baket = useSelector(selectorBasket);
  
const toggleForm = () => {
  dispatch(actionModalForm())
}

  return (
    <>
      <div className="form">
        <Formik
          initialValues={cvData}
          onSubmit={(values) => {
            dispatch(actionCvData(values));
            
            console.log("Дані користувача", values);
            console.log("Товари", baket);
            dispatch(actionCleanBasket());
            toggleForm()
          }}
          validationSchema={validationSchema}
        >
          {({ errors, touched, getFieldProps }) => (
            <Form>
              <fieldset className="form-block">
                <legend>Вкажіть свої дані для замовлення</legend>
                <Input
                  inputName="name"
                  label="Ім'я"
                  placeholder="Ім'я"
                  error={errors.name && touched.name}
                />
                <Input
                  inputName="lastName"
                  label="Прізвище"
                  placeholder="Прізвище"
                  error={errors.name && touched.lastName}
                />
                <Input
                  inputName="age"
                  label="Вік"
                  placeholder="Вік"
                  error={errors.name && touched.age}
                />
                <Input
                  inputName="region"
                  label="Адреса доставки"
                  placeholder="Адреса доставки"
                  error={errors.name && touched.region}
                />
                <Input
                  inputName="mobile"
                  label="Мобільний телефон"
                  placeholder="Мобільний телефон"
                  error={errors.name && touched.mobile}
                />
                <div className="order_button">
                  <Button
                    text="Створити замовлення"
                    type="submit"
                  />
                </div>
                <div className="buttons-wrap">
                <Close onClick={() => dispatch(actionModalForm())} />
                </div>
              </fieldset>
            </Form>
          )}
        </Formik>
      </div>
    </>
  );
};

export default FormOrder;
