import { render, screen } from "@testing-library/react";
import CardList from "./CardList";

describe("Cardlist", () => {
    test("Snapshot CardList", () => {
        const list = render(<CardList currentCard={{}}/>)
        expect(list).toMatchSnapshot()
    })

    test("Checked button", () => {
        const { container } = render(<CardList currentCard={{}} />);
        const button = container.querySelector(".button");
        expect(button).toBeInTheDocument();
      });
    
      test("Checked Favorite Icon", () => {
        const { container } = render(<CardList currentCard={{}} />);
        const heart = container.querySelector(".heart");
        expect(heart).toBeInTheDocument();
      });
    
})
