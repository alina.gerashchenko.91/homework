import { render, renderHook, screen, fireEvent, waitFor } from "@testing-library/react";
import { useSelector } from "react-redux";
import React from "react";
import { Provider } from "react-redux";
import store from "../../store";
import { selectorCountBasket, selectorCountFavorite, selectorFavorite } from "../../selectors";
import Card from "./Card";



describe("Card", () => {
    

  test("Snapshot Card", () => {
    const card = render(
      <Provider store={store}>
        <Card currentCard={{}} />
      </Provider>
    );

    expect(card).toMatchSnapshot();

  });

  test("Checked button", () => {
    const { container } = render(
      <Provider store={store}>
        <Card currentCard={{}} />
      </Provider>
    );
    const button = container.querySelector(".button");
    expect(button).toBeInTheDocument();
  });

  test("Checked Favorite Icon", () => {
    const { container } = render(
      <Provider store={store}>
        <Card currentCard={{}} addToFavorites={() => {}} />
      </Provider>
    );
    const heart = container.querySelector("svg");
    
    expect(heart).toBeInTheDocument();
    expect(heart).toHaveAttribute("fill", "gray");
    fireEvent.click(heart);
    expect(heart).toHaveAttribute("fill", "pink")
  });


});

