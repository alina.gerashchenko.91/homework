import Heart from "../icons/Heart";
import Basket from "../icons/Basket";
import Navigation from "../Navigation";
import PropTypes from "prop-types";

import { useSelector } from "react-redux";
import { selectorCountBasket, selectorCountFavorite, selectorFavorite, selectorBasket } from "../../selectors";
import {actionCountFavorite, actionCountBasket } from "../../actions"

import "./Header.scss";
import { useEffect } from "react";
import { useDispatch } from "react-redux";

function Header() {
  const favorite = useSelector(selectorFavorite);
  const basket = useSelector(selectorBasket);
  const countFavorite = useSelector(selectorCountFavorite);
  const countBasket = useSelector(selectorCountBasket)
  const dispatch = useDispatch();
  
  useEffect(() => {

    if (JSON.parse(localStorage.getItem("favorite"))) {
      dispatch(actionCountFavorite());
    }

    if (JSON.parse(localStorage.getItem("basket"))) {
      dispatch(actionCountBasket());
    }
}, [favorite, basket])


  return (
    <div className="header">
      <Navigation />
      <div className="header__icons">
      <Basket actions={<p className="basket_counter">{countBasket}</p>} />
      <Heart actions={<p className="heart_counter">{countFavorite}</p>} />
      </div>
    </div>
  );
}

Header.protoTypes = {
  countBasket: PropTypes.number,
  countHeart: PropTypes.number
}

Header.defaultProps = {
  countBasket: 0,
  countHeart: 0
}

export default Header;
