import {render} from "@testing-library/react";
import Modal from "./Modal";

describe("Snapshot Modal", () => {
    test("Modal render", () => {
        const modal = render(<Modal/>)
        expect(modal).toMatchSnapshot()
    })

    test("Checked toBeInDoc", () => {
        const {container} = render(<Modal/>)
        expect(container).toBeInTheDocument()
    })
})
