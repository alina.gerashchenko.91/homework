
import {NavLink} from "react-router-dom";


import "./Navigation.scss"

function Navigation () {
    return <ul className="header__nav">
    <li className="nav-item">
        <NavLink to="/"
        className={({isActive}) => (isActive ? "nav-link active" : 'nav-link')}
        >
            Головна
        </NavLink>
    </li>
    <li className="nav-item">
        <NavLink to="/basket" 
        className={({isActive}) => (isActive ? "nav-link active" : 'nav-link')}
        >
            Кошик
        </NavLink>   
    </li>
    <li className="nav-item">
        <NavLink to="/favorites" 
        className={({isActive}) => (isActive ? "nav-link active" : 'nav-link')}
        >
            Обране
        </NavLink>
    </li>
    </ul>
}

export default Navigation;