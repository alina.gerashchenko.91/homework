import * as yup from "yup";

export const validationSchema = yup.object({
    name: yup
        .string("Введіть своє ім\`я")
        .required("Будь ласка, заповніть це поле")
        .min(2, "Ім\`я занадто коротке"),
    lastName: yup
    .string("Введіть своє прізвище")
    .required("Будь ласка, заповніть це поле")
    .min(2, "Прізвище занадто коротке"),
    age: yup
    .number()
    .required("Будь ласка, заповніть це поле")
    .positive(),

    region: yup
    .string("Введіть свою адресу")
    .required("Будь ласка, заповніть це поле")
    .min(4, "Адреса занадто коротка"),

    mobile: yup 
    .string("Введіть свій мобільний")
    .required("Будь ласка, заповніть це поле") 
})