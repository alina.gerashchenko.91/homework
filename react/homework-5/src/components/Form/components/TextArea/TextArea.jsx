import { Field, ErrorMessage } from "formik";
import cx from "classnames";
import PropTypes from "prop-types";

// const Textarea = ({label, className, error,textAreaName, rows, placeholder, ...restProps})=>{
//     return(
//         <label className={cx("form-item", className,{'has-validation':error})}>
//             <p className="form-label">{label}</p>
//             <Field as="textarea" className="form-control" name={textAreaName} rows={rows} placeholder={placeholder} {...restProps} />
//             <ErrorMessage className='error-message' name={textAreaName} component='p'/>
//     </label>
//     )
// }

// Textarea.propTypes = {
//     placeholder: PropTypes.string,
//     rows: PropTypes.number,
//     label: PropTypes.string,
//     textAreaName: PropTypes.string,
//     className: PropTypes.string,
//     error: PropTypes.object,
// }

const TextArea = ({
  label,
  className,
  error,
  textAreaName,
  rows,
  placeholder,
  ...restProps
}) => {
  return (
    <label className={cx("form-item", className, { "has-validation": error })}>
      <p className="form-label">{label}</p>
      <Field
        as="textarea"
        className="form-control"
        name={textAreaName}
        rows={rows}
        placeholder={placeholder}
        {...restProps}
      ></Field>
      <ErrorMessage
        className="error-message"
        name={textAreaName}
        component="p"
      />
    </label>
  );
};

TextArea.propTypes = {
  placeholder: PropTypes.string,
  rows: PropTypes.number,
  label: PropTypes.string,
  textAreaName: PropTypes.string,
  className: PropTypes.string,
  error: PropTypes.object,
};

export default TextArea;
