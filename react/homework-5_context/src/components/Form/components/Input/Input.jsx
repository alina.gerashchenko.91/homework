import { Field, ErrorMessage } from "formik";
import cx from "classnames";
import PropTypes from "prop-types";

import "./Input.scss";

const Input = ({
  className,
  label,
  type,
  inputName,
  placeholder,
  error,
  ...restProps
}) => {
  return (
    <>
      <label
        className={cx("form-item", className, { "has-validation": error })}
      >
        <p className="form-label">{label}</p>
        <Field
          type={type}
          className="form-control"
          name={inputName}
          placeholder={placeholder}
          {...restProps}
        />
        <ErrorMessage
          className="error-messege"
          name={inputName}
          component={"p"}
        />
      </label>
    </>
  );
};

Input.defautProps = {
  type: "text",
};
Input.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  inputName: PropTypes.string,
  label: PropTypes.string,
  className: PropTypes.string,
  error: PropTypes.object,
};

export default Input;
