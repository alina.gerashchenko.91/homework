import './EmptyPage.scss'


function EmptyPage ({text}) {
    return (
        <div className="empty">
            <p>Тут будуть усі товари{text}</p>
        </div>
    )
}

export default EmptyPage;