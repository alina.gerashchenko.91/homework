import { useSelector, useDispatch } from "react-redux";
import { useEffect} from "react";
import PropTypes from "prop-types";

import Card from "../../components/Card/Card";
import EmptyPage from "../../components/EmptyPage";

//import { actionFetchCards } from "../../actions";
import { selectorCards, selectorCardsLoading } from "../../selectors";
import {cardsReducer} from '../../reducers'
import actionModalSecond, {actionFetchCards} from '../../reducers'

function Cards({handlerCard, addToFavorites, modalOpen}) {
  
  const cards = useSelector(selectorCards);
  const loading = useSelector(selectorCardsLoading);
  const dispatch = useDispatch()

  const modalSecondToggle = () => {
    dispatch(actionModalSecond());
  };

  useEffect(() => {
    dispatch(actionFetchCards())
    console.log(cards);
    //cardsReducer()
  }, [])

  const cardItem = cards.map((card) => (
    <Card
      key={card.id}
      currentCard={card}
      handlerCard={handlerCard}
      addToFavorites={() => addToFavorites(card)}
      modalOpen={() => {modalSecondToggle()}}
    />
  ));

  return (<>{loading ? (
    <EmptyPage text=", які ви зможете додати до кошика чи обраного" />
  ) : <div className="products">{cardItem}</div>}</>);
}

Cards.propTypes = {
  handlerBasket: PropTypes.func,
  addToFavorites: PropTypes.func,
  modalOpen: PropTypes.func,
  handlerCard: PropTypes.func,
};

export default Cards;
