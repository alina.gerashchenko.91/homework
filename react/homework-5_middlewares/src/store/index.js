import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import {logger} from "redux-logger"
//import rootReducer from "../reducers";
import {cardsReducer} from "../reducers";
import thunk from "redux-thunk";

const store = configureStore({ 
    reducer: {
        cards: cardsReducer,
    }, 
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger, thunk)
});

export default store;
