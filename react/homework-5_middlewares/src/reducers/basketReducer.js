import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import sendRequest from "../helpers";


const initialState = {
    basketArray: JSON.parse(localStorage.getItem("basket")) || [],
    loading: true,
  };

export const basketSlice = createSlice({
     name: "basket",
     initialState,
     reducers: {
       actionBasket: (state, { payload }) => {
        state.basketArray = [...payload];
       },
       actionLoading: (state, { payload }) => {
         state.loading = payload;
       },
       actionAddToBasket: (state, { payload }) => {
        if (!state.basketArray.find((item) => item.id === payload.id)) {
            state.basketArray = [...state.basket, payload];
        } else {
            alert(
                "Товар вже додано до кошика. Ви можете змінити кількість товарів у відповідному розділі"
              );
        }
       },
       actionDeleteBasket: (state, {payload}) => {
        state.basketArray = state.basketArray.filter((item) => item.id !== payload.id);
       }

     },
   });
  
   export const actionFetchBasket = () => async (dispatch) => {
     dispatch(actionLoading(true));
     return await sendRequest(JSON.parse(localStorage.getItem("basket")) || [])
       .then((data) => {
               dispatch(actionBasket(data));
               dispatch(actionLoading(false));
           }
       );
   };
  
   export const { actionBasket, actionLoading } = basketSlice.actions;
  
   export default basketSlice.reducer;
  