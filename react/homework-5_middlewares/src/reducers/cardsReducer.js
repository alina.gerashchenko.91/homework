import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import sendRequest from "../helpers";

const initialState = {
  cardsArray: [],
  loading: true,
};



export const cardSlice = createSlice({
  name: "cards",
  initialState,
  reducers: {
    actionCards: (state, { payload }) => {
      state.cardsArray = [...payload];
    },
    actionLoading: (state, { payload }) => {
      state.loading = payload;
    },
  },

});

export const actionFetchCards = () => async (dispatch) => {
  dispatch(actionLoading(true));
  return await sendRequest(`${process.env.PUBLIC_URL}/productsArray.json`)
    .then((data) => {
            dispatch(actionCards(data));
            dispatch(actionLoading(false));
        }
    );
};

export const { actionCards, actionLoading } = cardSlice.actions;

export default cardSlice.reducer;
