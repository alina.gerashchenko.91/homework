//TABS OUR SEVICES

const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsItem = document.querySelectorAll(".tabs-item");

tabsTitle.forEach(function (e) {
  e.addEventListener("click", () => {
    const tabsId = e.getAttribute("data-tab");

    const activeItem = document.querySelector(tabsId);

    tabsTitle.forEach(function (e) {
      e.classList.remove("active");
    });

    tabsItem.forEach(function (e) {
      e.classList.add("hide");
    });

    e.classList.add("active");
    activeItem.classList.remove("hide");
  });
});

//OUR AMAZING WORK 

$(function () {
  const filter = $("[data-img-tab]");

  filter.on("click", function () {
    $(".show").not(this).removeClass("show");
    $(this).toggleClass("show");

    const category = $(this).data("img-tab");

    if (category == "all") {
      $("[data-img]").removeClass("hide");
    } else {
      $("[data-img]").each(function () {
        const valueCategory = $(this).data("img");

        if (valueCategory != category) {
          $(this).addClass("hide");
        } else {
          $(this).removeClass("hide");
        }
      });
    }
  });
});



//OUR AMAZING WORK 

const imgs = [
  "./images/our-work/graphic-design/graphic-design2.jpg",
  "./images/our-work/wordpress/wordpress3.jpg",
  "./images/our-work/web design/web-design4.jpg",
  "./images/our-work/landing page/landing-page5.jpg",
  "./images/our-work/graphic-design/graphic-design3.jpg",
  "./images/our-work/wordpress/wordpress4.jpg",
  "./images/our-work/web design/web-design5.jpg",
  "./images/our-work/graphic-design/graphic-design4.jpg",
  "./images/our-work/wordpress/wordpress5.jpg",
  "./images/our-work/web design/web-design6.jpg",
  "./images/our-work/graphic-design/graphic-design5.jpg",
  "./images/our-work/graphic-design/graphic-design6.jpg",
];

const showMore = document.querySelector(".green-btn");
showMore.addEventListener("click", function () {
  imgs.forEach(function (src) {
    const newImg = document.createElement("img");
    newImg.src = src;
    const newDiv = document.createElement("div");
    document.querySelector(".tabs-img-content").append(newDiv);
    newDiv.append(newImg);
  });
  showMore.classList.add("hide");
});

//REVIEW SLIDER 

$(document).ready(function () {
  $(".slider-for").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: ".slider-nav",
  });
  $(".slider-nav").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: ".slider-for",
    dots: false,
    centerMode: true,
    focusOnSelect: true,
  });
});

$(document).ready(function () {
  $(".carousel-item").on("click", function () {
    $(".up").not(this).removeClass("up");
    $(this).toggleClass("up");

    console.log($(this).data("slick-index"));
  });
});
